const express = require("express");
const expresshb = require('express-handlebars');
const bodyParser = require('body-parser');
const path = require('path');
const serverless = require('serverless-http');
//const db = require('./config/database');
const app = express();



// Handlebars
app.engine('handlebars', expresshb({ defaultLayout: 'main' }));
app.set('view engine', 'handlebars');
app.use(express.static(path.join(__dirname,'public')));
const port = process.env.port || 5000;

// gig routes
//app.use('/gigs', require('./routes/gigs'));
// support parsing of application/json type post data

const router = express.Router();
//const router = require('./routes/gigs');
router.get('/',(req,resp) => {

    //resp.render("index", { layout: 'landing'} );
    resp.send("Working!");
})
app.use('/.netlify/functions/api',router)
module.exports.handler = serverless(app);
//app.listen(port, console.log(`Server started at port ${port}`))

